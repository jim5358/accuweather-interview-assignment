﻿using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows;

namespace IPA.Conversions
{
    /// <summary>
    /// This class converts a text value of seconds to a duration object
    /// </summary>
    public class TextToDuration : IValueConverter
    {
        /// <summary>
        /// This converts from Duration to total seconds of the duration
        /// </summary>
        /// <param name="value">This is a Duration value</param>
        /// <param name="targetType">Double or String only</param>
        /// <param name="parameter">N/A</param>
        /// <param name="culture">N/A</param>
        /// <returns>Double value of the total seconds in the duration or null</returns>
        public object Convert(object value, Type targetType, 
            object parameter, CultureInfo culture)
        {
            if(!(value is Duration))
                return null;

            if(!targetType.Equals(typeof(Double)) && 
                !targetType.Equals(typeof(Nullable<Double>)) && 
                !targetType.Equals(typeof(String)))
                return null;

            return ((Duration) value).TimeSpan.TotalSeconds;
        }
 
        /// <summary>
        /// This converts from text to a duration, if no errors arise
        /// </summary>
        /// <param name="value">The text to be converted</param>
        /// <param name="targetType">Duration only</param>
        /// <param name="parameter">N/A</param>
        /// <param name="culture">N/A</param>
        /// <returns>Duration of the text or null</returns>
        public object ConvertBack(object value, Type targetType, 
            object parameter, CultureInfo culture)
        {
            if(!(value is String))
                return null;

            if(!targetType.Equals(typeof(Duration)) && 
                !targetType.Equals(typeof(Nullable<Duration>)))
                return null;

            String text = value as String;

            int duration;
            Boolean successParse = int.TryParse(text, out duration);
            if(!successParse || duration <= 0)
                return null;

            TimeSpan ts = new TimeSpan(0, 0, duration);
            return new Duration(ts);
        }
    }
}
