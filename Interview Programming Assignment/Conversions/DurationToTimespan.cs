﻿using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows;

namespace IPA.Conversions
{
    /// <summary>
    /// This class converts a duration to a timespan
    /// </summary>
    public class DurationToTimespan : IValueConverter
    {
        /// <summary>
        /// This converts a duration to a timespan for the beginning time of
        /// different segments of the animation
        /// </summary>
        /// <param name="value">The duration</param>
        /// <param name="targetType">TimeSpan only</param>
        /// <param name="parameter">This is the key frame value to shift the start time by parameter * value</param>
        /// <param name="culture">N/A</param>
        /// <returns>TimeSpan of Duration ( value ) * key frame ( parameter ) or null</returns>
        public object Convert(object value, Type targetType, 
            object parameter, CultureInfo culture)
        {
            if(!(value is Duration))
                return null;

            if(!targetType.Equals(typeof(Duration)) && 
                !targetType.Equals(typeof(Nullable<TimeSpan>)))
                return null;

            TimeSpan durationSpan = ((Duration) value).TimeSpan;

            int startParam;
            Boolean successParse = int.TryParse(parameter as String, out startParam);
            if(!successParse || startParam < 0)
                startParam = 0;

            startParam *= (int)durationSpan.TotalSeconds;

            TimeSpan startTime = new TimeSpan(0, 0, startParam);

            return startTime;
        }
 
        /// <summary>
        /// Not Implemented - Not necessary for this program
        /// </summary>
        public object ConvertBack(object value, Type targetType, 
            object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
