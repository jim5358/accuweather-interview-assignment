﻿using System;
using System.Windows;
using System.ComponentModel;

namespace IPA.Models
{
    /// <summary>
    /// This class models the different settings for the Animation label that are changeable
    /// </summary>
    public class PModel : INotifyPropertyChanged
    {
        // Handle the event that a property changes in order to update the bindings
        public event PropertyChangedEventHandler PropertyChanged;

        // The private variables
        private String _title;
        private int _textSize;
        private Duration _animationLength;

        /// <summary>
        /// The text of the animation label
        /// </summary>
        public String Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                raisePropertyChanged("Title");
            }
        }

        /// <summary>
        /// The text size for the animation label
        /// </summary>
        public int TextSize
        {
            get
            {
                return _textSize;
            }
            set
            {
                _textSize = value;
                raisePropertyChanged("TextSize");
            }
        }

        /// <summary>
        /// The length of one key frame of the animation
        /// </summary>
        public Duration AnimationLength
        {
            get
            {
                return _animationLength;
            }
            set
            {
                _animationLength = value;
                raisePropertyChanged("AnimationLength");
            }
        }

        /// <summary>
        /// This simply causes the program to realize that a property has been changed
        /// </summary>
        /// <param name="propertyName">The name of the property</param>
        private void raisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) 
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// This creates a Properties Model object with the given parameters
        /// </summary>
        /// <param name="title">The animation text</param>
        /// <param name="textSize">The animation text size</param>
        /// <param name="seconds">The seconds between each key frame</param>
        public PModel(String title, int textSize, int seconds)
        {
            this.Title = title;
            this.TextSize = textSize;
            this.AnimationLength = new Duration(new TimeSpan(0, 0, seconds));
        }
    }
}
