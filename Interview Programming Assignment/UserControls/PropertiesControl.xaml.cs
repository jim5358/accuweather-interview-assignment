﻿using IPA.Conversions;
using IPA.Models;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace IPA.UserControls
{
    /// <summary>
    /// Interaction logic for PropertiesControl.xaml
    /// </summary>
    public partial class PropertiesControl : UserControl
    {
        public PropertiesControl()
        {
            InitializeComponent();

            // A conversion class
            TextToDuration ttd = new TextToDuration();

            // Bind all the necessary controls to the correct properties in the PModel which is implicit by the DataContext
            this.Input.SetBinding(TextBox.TextProperty, new Binding("Title") { Mode = BindingMode.TwoWay } );
            this.TextSize.SetBinding(ComboBox.TextProperty, new Binding("TextSize") { Mode = BindingMode.TwoWay } );
            this.AnimDuration.SetBinding(TextBox.TextProperty, new Binding("AnimationLength") { Mode = BindingMode.TwoWay, Converter =  ttd} );
        }

        /// <summary>
        /// This function resets the fields, mostly it's a way to test that the property notifications are set up correctly
        /// </summary>
        /// <param name="sender">The reset button, but it doesn't matter much</param>
        /// <param name="e">Probably a click event, but it is irrelevant</param>
        private void ResetButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PModel context = (PModel) this.DataContext;
            context.Title = "AccuWeather";
            context.TextSize = 48;
            context.AnimationLength = new Duration(new TimeSpan(0, 0, 2));
        }
    }
}
