﻿using System.Windows;

using IPA.Models;
using IPA.UserControls;

namespace IPA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // Initialize a properties model with some default values
            PModel pm = new PModel("AccuWeather", 48, 2);

            // Set the Data Context for both controllers to be the properties model
            this.PropControl.DataContext = pm;
            this.AnimControl.DataContext = pm;
        }
    }
}
